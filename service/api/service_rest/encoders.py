from common.json import ModelEncoder

from .models import AutomobileVO, Technician, Appointment

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO,
    properties = ["vin", "import_href"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "id",
        "vin",
        "technician"
    ]

    def get_extra_data(self, o):
        return {"vin": o.vin, "technician": o.technician.name}

    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        return {"vin_in_system": count > 0}

    encoders = {
        "technician": TechnicianEncoder(),
    }
