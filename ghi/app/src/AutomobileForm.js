import React, {useEffect, useState} from 'react';

function AutomobileForm () {
    // const [color, setColor] = useState("");
    // const [year, setYear] = useState("");
    // const [vin, setVin] = useState("");
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: "",
        year: "",
        vin: "",
        model: "",
    })

    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url)
        if (response.ok){
            const data = await response.json()
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData();
    },[]);


    const handleSubmit = async (event) => {
        event.preventDefault();


    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(autoUrl, fetchConfig)
    if (response.ok){
        setFormData({
            color: "",
            year: "",
            vin: "",
            model: "",
        });

        window.location.href = 'http://localhost:3000/automobiles/'
    }
}

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Automobile</h1>
                <form onSubmit={handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" value={formData.color} id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange}placeholder="year" required type="text" name="year" value={formData.year} id="year" className="form-control"/>
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange}placeholder="vin" required type="text" name="vin" value={formData.vin} id="vin" className="form-control"/>
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <select onChange={handleFormChange} required name="model_id" id="model_id" value={formData.model_id} className="form-select">
                            <option value="">Choose a model...</option>
                            {models?.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                )
                            })}
                    </select>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
}
export default AutomobileForm;
