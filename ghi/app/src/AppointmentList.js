import React from 'react';

function AppointmentList(props) {


    const handleCancel = async (id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel`;
        try {
            const response = await fetch(cancelUrl, {method: 'PUT'});
            if (response.ok) {
                window.location.reload();
            }
        } catch (error) {
            console.error('Could not cancel appointment:', error);
        };
    }

    const handleFinish = async (id) => {
        const finishUrl = `http://localhost:8080/api/appointments/${id}/finish`;
        try {
            const response = await fetch(finishUrl, {method: 'PUT'});
            if (response.ok) {
                window.location.reload();
            }
        } catch (error) {
            console.error('Could not finish appointment:', error);
        };
    }

    return (
            <div className="text-center">
                <h1 className="display-7 fw-bold">Appointments</h1>
                <table className="table table-hover table-bordered border-dark">
                    <thead>
                        <tr className="table-success table-bordered border-dark">
                            <th>Vin</th>
                            <th>VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Reason</th>
                            <th>Technician</th>
                            <th>Update Appointment</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.appointments.filter(appointment => appointment.status === "created").map(appointment => {
                            const date = new Date(appointment.date_time).toLocaleDateString({hour12: true});
                            const time = new Date(appointment.date_time).toLocaleTimeString([], {hour: "2-digit", minute: "2-digit"});
                            return (
                                <tr key={appointment.id}>
                                    <td className="fw-bold">{appointment.vin}</td>
                                    <td>{appointment.vin_in_system ? "Yes" : "No"}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{date}</td>
                                    <td>{time}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>
                                        <button className="btn btn-outline-warning fw-bold" onClick={() => handleCancel(appointment.id)}>Cancel</button>
                                        <button className="btn btn-outline-success fw-bold" onClick={() => handleFinish(appointment.id)}>Finish</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
}

export default AppointmentList;
