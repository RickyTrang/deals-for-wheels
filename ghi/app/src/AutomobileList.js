import React, {useEffect, useState,} from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }
    

    useEffect(() => {
        fetchData();
    },[]);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Color</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>VIN</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {automobiles?.map(automobile => {
                    return (
                        <tr key={automobile.id}>
                            <td>{automobile.year}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.vin}</td>
                            <td>{automobile.model.picture_url}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}
export default AutomobileList;
