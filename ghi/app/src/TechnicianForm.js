import React, { useState } from 'react';

function TechnicianForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

    const url = 'http://localhost:8080/api/technicians/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      window.location.href = 'http://localhost:3000/technicians/';
    }
  }

  const handleFirstName = (e) => {
    const value = e.target.value;
    setFirstName(value);
  }
  const handleLastName = (e) => {
    const value = e.target.value;
    setLastName(value);
  }
  const handleEmployeeId = (e) => {
    const value = e.target.value;
    setEmployeeId(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">

            <div className="form-floating mb-3">

              <input onChange={handleFirstName} placeholder="First Name" required type="text" name="first_name" value={firstName.first_name} id="first_name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleLastName} placeholder="Last Name" required type="text" name="last_name" value={lastName.last_name} id="last_name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleEmployeeId} placeholder="Employee Id" required type="text" name="employee_id" value={employeeId.employee_id} id="employee_id" className="form-control" />
              <label htmlFor="employee_id">Employee Id</label>
            </div>
            <button className="btn btn-lg btn-primary">Add Technician</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default TechnicianForm;
