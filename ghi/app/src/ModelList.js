import React, { useState, } from 'react';

function ModelList(props) {
    const [models, setModels] = useState(props.models);

    const deleteModel = async (id) => {
      const url = `http://localhost:8100/api/models/${id}/`;
      try {
        const response = await fetch(url, {
            method: 'DELETE'
        });
        if (response.ok) {
            setModels(models.filter((model) => model.id !== id));
            window.location.reload();
        }
    } catch (error) {
        console.error('Could not cancel appointment:', error);
        };
    }

        return (
            <div className="text-center">
                <h1 className="display-6 fw-bold">Models</h1>
                <table className="table table-hover table-bordered border-dark">
                    <thead>
                        <tr className="table-success table-bordered border-dark">
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img src={model.picture_url} alt="Model" style={{width: '140px', height: '75px'}}></img></td>
                                    <td>
                                        <button className="btn btn-outline-danger fw-bold" onClick={() => deleteModel(model.id)}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

export default ModelList;
