import React, { useState } from 'react';

function SalesPeopleForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId
    const url = `http://localhost:8090/api/salespeople/`;

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        window.location.href = "http://localhost:3000/salespeople"

    }
}

const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
}
const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
}
const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
}


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-salespeople-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" value={firstName.first_name} id="first_name" className="form-control"/>
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange}placeholder="Last Name" required type="text" name="last_name" value={lastName.last_name} id="last_name" className="form-control"/>
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIdChange}placeholder="Employee Id" required type="text" name="employee" value={employeeId.color} id="employee_id" className="form-control"/>
                        <label htmlFor="Employee Id">Employee Id</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }

export default SalesPeopleForm;
