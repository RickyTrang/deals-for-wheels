import React, { useEffect, useState } from 'react';

function AppointmentForm() {
  const [technicians, setTechnicians] = useState([])

  const [formData, setFormData] = useState({
    customer: '',
    date_time: '',
    reason: '',
    vin: '',
    technician: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {

      setFormData({
        customer: '',
        date_time: '',
        reason: '',
        vin: '',
        technician: '',
      });
      window.location.href = 'http://localhost:3000/appointments/';
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">

            <div className="form-floating mb-3">

              <input onChange={handleFormChange} placeholder="Customer Name" required type="text" name="customer" value={formData.customer} id="customer" className="form-control" />
              <label htmlFor="name">Customer Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Date & Time" required type="text" name="date_time" value={formData.date_time} id="date_time" className="form-control" />
              <label htmlFor="date_time">Date & Time</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" value={formData.reason} id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Vin" required type="text" name="vin" value={formData.vin} id="vin" className="form-control" />
              <label htmlFor="vin">Vin</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} required name="technician" value={formData.technician} id="technician" className="form-select">
                <option value="">Assign a Technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-lg btn-success">Create Appointment</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm;
