import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadProject() {
  const technicianResponse = await fetch('http://localhost:8080/api/technicians/');
  const appointmentResponse = await fetch('http://localhost:8080/api/appointments/');
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const modelResponse = await fetch('http://localhost:8100/api/models/');
  const customersResponse = await fetch ("http://localhost:8090/api/customers/");

  if (technicianResponse.ok && appointmentResponse.ok &&
    manufacturerResponse.ok && modelResponse.ok && customersResponse.ok) {
    const technicianData = await technicianResponse.json();
    const appointmentData = await appointmentResponse.json();
    const manufacturerData = await manufacturerResponse.json();
    const modelData = await modelResponse.json();
    const customerData = await customersResponse.json();

    return root.render(
      <React.StrictMode>
        <App technicians={technicianData.technicians}
        appointments={appointmentData.appointments}
        manufacturers={manufacturerData.manufacturers}
        models={modelData.models}
        customers={customerData.customers}/>
      </React.StrictMode>
    );
  } else {
    console.error(technicianResponse);
    console.error(appointmentResponse);
    console.error(manufacturerResponse);
    console.error(modelResponse);
    console.error(customersResponse);
  }
}
loadProject();
